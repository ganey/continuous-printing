This repo is for a piece of software I wrote to assist with continuous printing by verifying that the prints ejected correctly using motion detection. It also might set fire to your printer. YMMV, no warranty, etc. A video demonstration is available at https://youtu.be/DaG44rSTKPw

Requirements:
---
- An i3-style 3D printer, mine is an MK3S. You may be able to do this on other printers.
- Octoprint
- A webcam

Setting up the GCode:
---
Don't just use my gcode (also don't download and run gcode from other random people on the internet in general, you can damage your printer). My gcode is included as an example of how to edit your files, it is not meant for you to download run on your printer, especially if your printer is not the same as mine. Slice it as you would normally in your own slicer, then read edit it using the instructions below.

I did this in Prusa Slicer, so this may differ to your slicer. After slicing the gcode, I made two copies, one for the first print, one for the repeating prints.

In the first print file, I edited out the 2 cooldown lines at the end of the file. Leave the bed cooldown in place if you intend to use the eject_bed_temp feature.:
```
M104 S0 ; turn off temperature
M107 ; turn off fan
```

An example of this is available in example_code/first.gcode

In the repeat print file, I edited out both the 2 cooldown lines, and (most importantly) the priming line. Prusa Slicer comments them with "intro line" so you can find them fairly easily near the top of the file. I edited them to be a wipe instead of a priming line.

Before, the two lines were:

```
G1 X60.0 E9.0 F1000.0 ; intro line
G1 X100.0 E12.5 F1000.0 ; intro line
```

After, they are now
```
G0 X60.0 ; intro line
G0 X100.0 ; intro line
```

Next, we need to set up the ejection gcode. There's an example of my ejection gcode at example_gcode/eject.gcode. The technique I have used is to move the hotend to the back of the print, and slowly move it forwards and upwards to lift the print off the bed. This seems to work very well. You will need to adjust the values if you do not have a 25 x 21 x 21 cm bed.

Once you have your 3 gcode files, upload them all to octoprint.

Setting up the code:
---
Copy config.yaml.example to config.yaml and edit the values as appropriate. Note that you should use the file name not the display name from octoprint. You can get the file name by pressing the download button in the file list in Octoprint.

Once you have done that, run `pip install -r requirements.txt`

Next, test that your camera / motion detection is working by running `python go.py --test-motion`. Drop a shield in front of your camera to test it. If you get motion detected, it's working. If you don't get motion detected, try lowering motion_threshold. If you get motion detected when you shouldn't, try increasing motion_threshold.

Now you are ready to go. Run `python go.py` and it will begin. If you are already printing, feel free to run `python go.py --already-printing` to skip the first print, and wait for the current one to finish.
